

Este repositorio se lo utiliza para llevar un registro de lo realizado en el trabajo final, que debe contar con una aplicacion generada en app inventor. para llevar a cabo un trabajo en equipo, coordinando las tareas a realizar usando la aplicacion Trello, que nos permite organizarnos y qué tareas le corresponde a cada integrante del equipo.


Nuestra aplicación creada es una Lista del supermercado, para facilitar tener un registro de lo que se debe adquirir cuando se realizan las compras de productos y alimentos necesarios para abastecer un hogar, y no olvidarse de nada indispensable.
A las imágenes buscadas, la trabajamos para sacarle el color de fondo con la aplicación Remove Background (removebg), y así colocarlo para que quede transparente.

Adjunto en link de la aplicación Trello, usada para la asignación de las tareas  https://trello.com/b/y03CHQVC/trabajo-final





